import Grid from '@material-ui/core/Grid';
import React from 'react'
import { useStyle } from '../../App'

function RandomImageGenerator() {
    const classes = useStyle();
    return (
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
    )
}

export default RandomImageGenerator
