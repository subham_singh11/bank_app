import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Grid from '@material-ui/core/Grid'
import { useStyle } from '../../App'
import Paper from '@material-ui/core/Paper';
import RandomImageGenerator from '../RandomImageGenerator/RandomImageGenerator';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import ButtonComponent from '../ButtonComponent/ButtonComponent';
import Avatar from '@material-ui/core/Avatar';
import LockIcon from '@material-ui/icons/Lock';
import ReadOnlyTextComponent from '../ReadOnlyTextComponent/ReadOnlyTextComponent';

export default function LoginComponent() {
    const classes = useStyle();
    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <RandomImageGenerator />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockIcon />
                    </Avatar>
                    <ReadOnlyTextComponent
                        isBold
                        primary
                        size="large"
                        textColor="black"
                        textToBeInserted="Welcome to Sample Bank of India"
                    />
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        {/* <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={
                                () => {
                                    alert('Clicked')
                                }
                            }
                        >
                            Sign In
                        </Button> */}
                        <Grid container>
                            <Grid item xs>
                                <ButtonComponent
                                    primary={true}
                                    size='medium'
                                    label="Sign In"
                                    onClick={
                                        () => {
                                            alert('Clicked')
                                        }
                                    } ></ButtonComponent>
                            </Grid>
                            <Grid item>
                                <Link href="/signup" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    )
}

