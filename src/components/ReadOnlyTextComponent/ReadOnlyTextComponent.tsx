import React from 'react'
import './ReadOnlyTextComponent.css';

export interface ReadOnlyTextComponentProps {
    primary?: boolean;
    size?: 'small' |'smaller'| 'medium' | 'large';
    textColor?: string;
    textToBeInserted: string;
    isBold?: boolean;
}

const ReadOnlyTextComponent = (props: ReadOnlyTextComponentProps) => {
    const {
        primary = false,
        size = 'medium',
        textColor,
        textToBeInserted,
        isBold = false,
        ...rest
    } = props;

    const mode = primary ? 'text--primary' : 'text--secondary';

    return (
        <p
            className={[`text`, `text--${size}`, mode].join(' ')}
            style={{
                fontWeight: isBold ? 'bold' : 'normal',
                color: primary? "": textColor,
            }}
            {...rest}
        >
            {textToBeInserted}
        </p>
    )
}

export default ReadOnlyTextComponent
