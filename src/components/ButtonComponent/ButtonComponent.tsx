import React from 'react'
import './ButtonComponent.css'

export interface ButtonComponentProps {
    primary?: boolean;
    backgroundColor?: string;
    size?: 'small' | 'medium' | 'large';
    label: string;
    onClick?: () => void;
}

const ButtonComponent = (props: ButtonComponentProps) => {
    const {
        primary = false,
        size = 'medium',
        backgroundColor,
        label,
        ...rest
    } = props;

    const mode = primary ? 'button--primary' : 'button--secondary';

    return (
        <button
            type="button"
            className={['button', `button--${size}`, mode].join(' ')}
            style={{ backgroundColor }}
            {...rest}
        >
            {label}
        </button>
    )
}

export default ButtonComponent
