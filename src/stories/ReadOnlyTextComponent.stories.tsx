import React from 'react'
import { Story, Meta } from '@storybook/react'
import ReadOnlyTextComponent, { ReadOnlyTextComponentProps } from '../components/ReadOnlyTextComponent/ReadOnlyTextComponent'

export default {
    title: 'Form/ReadOnlyTextComponent',
    component: ReadOnlyTextComponent,
    // argTypes: {
    //     isBold: {control: ''}
    // }
}

const Template : Story<ReadOnlyTextComponentProps> = (args) => <ReadOnlyTextComponent {...args} />

export const Primary = Template.bind({})
Primary.args = {
    primary: true,
    textColor: 'black',
    textToBeInserted: 'Here is a Random Text',
    isBold: false,
}

export const Secondary = Template.bind({})
Secondary.args = {
    primary: false,
    textToBeInserted: "Here is Random Text",
    isBold: false,
}

export const Smaller = Template.bind({})
Smaller.args = {
    primary: true,
    size: 'smaller',
    textToBeInserted: 'Here is a Random Text',
    isBold: false,
}

export const Small = Template.bind({})
Small.args = {
    primary: true,
    size: 'small',
    textToBeInserted: 'Here is a Random Text',
    isBold: false,
}

export const Medium = Template.bind({})
Medium.args = {
    primary: true,
    size: 'medium',
    textToBeInserted: 'Here is a Random Text',
    isBold: false,
}

export const Large = Template.bind({})
Large.args = {
    primary: true,
    size: 'large',
    textToBeInserted: 'Here is a Random Text',
    isBold: false,
}