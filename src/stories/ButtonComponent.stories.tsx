import React from "react";
import { Story, Meta } from "@storybook/react";
import ButtonComponent, { ButtonComponentProps } from "../components/ButtonComponent/ButtonComponent";

export default {
    title: 'Form/ButtonComponent',
    component: ButtonComponent,
    argTypes: {
        backgroundColor: {control: 'color'}
    }
}

const Template : Story<ButtonComponentProps> = (args) => <ButtonComponent {...args} />

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Button',
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'Button',
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  label: 'Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Button',
};